class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.string :nameEvent
      t.string :startDate
      t.string :endDate
      t.string :address
      t.string :latitude
      t.string :longitude
      t.string :nameUstad
      t.string :theame
      t.string :comment

      t.timestamps
    end
  end
end
