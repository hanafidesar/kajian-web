json.extract! event, :id, :title, :nameEvent, :startDate, :endDate, :address, :latitude, :longitude, :nameUstad, :theame, :comment, :created_at, :updated_at
json.url event_url(event, format: :json)
